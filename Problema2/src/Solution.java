
import java.io.*;
import java.util.*;

public class Solution {

    // Complete the minimumNumber function below.
    static int minimumNumber(int n, String password) {
        int digitoFaltantes = 0;
        boolean tieneDigito = false, tieneMinuscula = false, tieneMayus = false, tieneEspecial = false;

        for (int i = 0; i < password.length(); i++) {
            int caracteres[] = new int[100];
            caracteres[i] = password.charAt(i);
            /*Separo el password en caracteres y se transforman a ASCII*/

            if (caracteres[i] >= 48 && caracteres[i] <= 57) {
                tieneDigito = true;
                /*
                 *Verifico si tiene, al menos, un Digito (numero).
                 *Del 0 al 9 en ASCII es 48-57.
                 */
            }
            if (caracteres[i] >= 97 && caracteres[i] <= 122) {
                tieneMinuscula = true;
                /*
                 *Verifico si tiene, al menos, una minuscula.
                 *De a a z en ASCII es 97-122.
                 */
            }
            if (caracteres[i] >= 65 && caracteres[i] <= 90) {
                tieneMayus = true;
                /*
                 *Verifico si tiene, al menos, una mayuscula.
                 *De A a Z en ASCII es 65-90.
                 */
            }
            if (caracteres[i] >= 35 && caracteres[i] <= 38 || caracteres[i] >= 40 && caracteres[i] <= 43 || caracteres[i] == 33 || caracteres[i] == 45 || caracteres[i] == 64 || caracteres[i] == 94) {
                tieneEspecial = true;
                /*
                 *Verifico si tiene, al menos, un caracter especial.
                 *La condicional (if) tiene todos los caracteres especiales con OR (porque no estan juntos).
                 */
            }

        }
        if (tieneDigito == false) {
            digitoFaltantes++;
        }
        if (tieneMinuscula == false) {
            digitoFaltantes++;
        }
        if (tieneMayus == false) {
            digitoFaltantes++;
        }
        if (tieneEspecial == false) {
            digitoFaltantes++;
        }
        /*
         *Si no se cumple alguno de los requisitos, se suma uno al contador de digitoFaltantes.
         *Estos if ya van fuera del for.
         */

        int largo = n + digitoFaltantes;
        while (largo < 6) {
            digitoFaltantes++;
            largo++;
        }
        /*
         *Al largo original del pasword se le suman los digitos faltantes para saber si asi ya cumple
         *el requisito de los 6 digitos.
         * Si no se cumple, se agregan digitos faltantes hasta que se cumpla la condicion de 6.
         */

        return digitoFaltantes;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String password = scanner.nextLine();

        int answer = minimumNumber(n, password);

        System.out.println(answer);

        scanner.close();
    }
}
